let bg = chrome.extension.getBackgroundPage();

let output: Element;
let input: HTMLInputElement;

// {icon, content, uri, id, priority}

let source = [];
let filtered = [];

let tab;
let lastValue;
function search(e) {
    let value = input.value;
    let query = encodeURIComponent(value);
    lastValue = value;
    bg.lastInput = value;
    let req = new XMLHttpRequest();
    let url = "http://localhost:8983/solr/web-index/select?hl.fl=content,title&hl=on&indent=on&q="+ query + "&wt=json";
    req.open("GET", url);
    req.addEventListener('load', (e) => {
        let response = JSON.parse(req.response);
        bg.console.log(response);
        if (response.responseHeader.params.q !== lastValue) {
            return;
        }
        bg.console.log("last: " + lastValue + " this: " + response.responseHeader.params.q);
        let highlights = response.highlighting;
        let documents = response.response.docs;
        let bar = {};
        output.innerHTML = "";
        for (let i in documents) {
            let doc = documents[i];
            let div = document.createElement('div');
            let description = document.createElement('p');
            let title = document.createElement('h5');
            div.className = 'item';
            bg.console.log(div);
            div.setAttribute("title", doc.url);

            let cachedContentUrl = "http://localhost:8000/"+doc.id+".html"
            div.onclick = () => {
                // let contentReq = new XMLHttpRequest();
                // contentReq.open("GET", "http://localhost:8000/"+doc.id+".html");
                // contentReq.onload = (e) => {
                //     document.documentElement.innerHTML = contentReq.response;
                // }
                // contentReq.send()

                window.open(doc.url, "_blank");
                // window.open(cachedContentUrl, "_blank")

                // document.documentElement.innerHTML = doc.content;
            }
            if (doc.title && doc.title[0]) {
                bg.console.log(doc.title[0]);
                title.innerHTML = doc.title[0];
            }
            if (highlights[doc.id]) {
                bg.console.log(highlights[doc.id]);
                description.innerHTML = highlights[doc.id].content[0].trim();
            }
            div.appendChild(title);
            div.appendChild(description);
            output.appendChild(div);
        }
    });
    req.send();
}

// {icon, content, uri, id, priority}

chrome.tabs.query({
    'active': true,
    'currentWindow': true
}, (tabs) => {
    if ('undefined' != typeof tabs[0].id && tabs[0].id) {
        tab = tabs[0];

        window.onload = function() {
            input = document.getElementsByClassName('qs_input')[0];
            input.addEventListener('input', search);
            output = document.getElementsByClassName('content')[0];
            if (bg.lastInput) {
                input.value = bg.lastInput;
                input.selectionStart = 0;
                input.selectionEnd = input.value.length;
                search(undefined);
            }
        };
    }
});

