chrome.runtime.onMessage.addListener(
    (message, sender, sendResponse) => {
        if (message.type == 'archive') {
            archive();
        }
    }
);

// document.addEventListener("DOMContentLoaded", (event) => {
//     console.log("archive page on load");
// });

function archive() {
    chrome.runtime.sendMessage({type: 'archive'
                                , time: Date.now()
                                , url: window.location.toString()
                                , title: document.title
                                , page: document.documentElement.outerHTML});
}

archive();
