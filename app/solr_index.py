#!/usr/bin/env python3

from glob import iglob
from urllib.parse import urlencode
import requests
import os.path
import sys
import json
import os

archive_path = os.environ["HOME"]+"/log/archive"
solr_core = "web-index"
solr_base_url = "http://localhost:8983/solr"+"/"+solr_core

def strip_extension(filename):
    dot_i = filename.rfind(".")
    if dot_i > 0:
        return filename[:dot_i]
    else:
        return filename

def index_page(html_filename, json_filename):

    with open(json_filename) as json_file:
        metadata = json.load(json_file)
        params = {
            "literal.id"    : strip_extension(os.path.basename(html_filename)),
            "literal.url"   : metadata["url"],
            "literal.title" : metadata["title"],
            "literal.date"  : metadata["time"],
            "commit"        : "true"
        }

        files = { "file": (html_filename,
                            open(html_filename, "rb"),
                            "text/html") }

        solr_url = solr_base_url+"/update/extract"

        requests.post(solr_url, files=files, params=params)
        # requests.post("http://localhost:8000", files=files, params=params)

if __name__ == '__main__':
    for html_filename in iglob(archive_path+"/*.html"):
        json_filename = strip_extension(html_filename)+".json"
        index_page(html_filename, json_filename)
    
